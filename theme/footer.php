<?php
/**
 * The Footer for the Baker Design Baseline Theme
 *
 *
 * @package WordPress
 * @subpackage Baker Design Theme 1.0
 * @since 2.0
 */
 ?>
</main> <!-- # end site container --> 

<footer id="site-footer">
	<p>&copy; <?php echo date('Y'); ?>  <?php echo bloginfo('name'); ?></p> <!-- copyright -->
</footer>

<?php wp_footer(); ?>
</body>
</html>