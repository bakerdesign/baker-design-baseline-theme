<?php

// List the directories to search in an array
global $directories;

// Create global flag so loopDir is only run once
global $loopDirFlag;

$loopDirFlag = true;

$directories = array(
	array(
		'keyword' 		=> 'module',
		'taxonomy_name'	=>	'module_type_taxonomy'
	)
	
);

// Creates the needed terms depending on the directories
function createTax( $value, $parent) {

	$parent = substr( $parent, strrpos( $parent, "/")+1);

	// Look to see if this is a parent or child term
	$term = get_term_by( 'slug', $parent, $value["taxonomy_name"]);

	// Default is set to parent term
	$temp = 0;
	if( $term) {
		// If not default, set parent id
		$temp = $term->term_id;
	}

	// Check to see if term exists
	if( !term_exists( $value['keyword'], $value['taxonomy_name'], array( 'parent' => $temp))) {

		$module_sass_string = "\n". '@import "../../templates/module/' . $value['keyword'] . '/module-style.scss";';
		$main_sass = __dir__ . '/assets/scss/app.scss';
		$get_sass_content = file_get_contents($main_sass);
		$get_sass_content .= $module_sass_string;
		
		file_put_contents($main_sass, $get_sass_content);

		// If term doesnt exist, insert the term
		wp_insert_term( $value['keyword'], $value['taxonomy_name'], array( 'parent' => $temp));
	}
}


function loopDir( $value, $inner = "/templates/", $parent = "") {

	if( !$value["keyword"]) {
		$value = $value[0];
	}

	// Get a current time to set all the timestamp
	$timeStamp = time();

	// Scan the directory to find all the files and subdirectories
	$files = scandir( __dir__ . $inner . $value["keyword"]);
	
	// Add value to temp for the inner directories
	$temp = $inner . $value['keyword'] . "/";

	// Loop through the results, calling this function when needed
	foreach ($files as $file) {


		// Check if the file is a special directory
		if( $file !== "." && $file !== ".." ) {

			// Check if this file is a directory
			if( is_dir( __dir__ . $temp . $file)) {
				
				touch( __dir__ . $temp . $file, $timeStamp);

				// call this function again if directory
				loopDir( array( 'keyword' => $file, 'taxonomy_name' => $value["taxonomy_name"]), $temp, $value["keyword"]);		

			}
			// Check if this file is css or js

		}
	
	}

}

// Get the Module Templates Directory
foreach ($directories as $directory) {

	// Scan directories for modified files
	scanDirectoriesForNewFiles( $directory);

}

function scanDirectoriesForNewFiles( $directory, $inner = "/templates/") {

	// Initiate the global variable
	global $directories;

	// Create the stat file if it is not already created
	if( !file_exists( __dir__ . $inner . $directory["keyword"] . "/stat.txt")) {
		fopen( __dir__ . $inner . $directory["keyword"] . "/stat.txt", "w");
	}	

	// Get the timestamp for the stat file
	$time = filemtime( __dir__ . $inner . $directory["keyword"] . "/stat.txt");

	// Scan the files
	$files = scandir( __dir__ . $inner . $directory["keyword"]);

	// Loop through the files

	foreach ($files as $file) {

		// Call the inner loop function, saving the return value
		$flag = scanLoop( $directory, $inner, $file, $time);

		// echo __dir__ . $inner . $directory["keyword"];
		// // Write the contents back to the file
		// file_put_contents($file, $current);

		// Initate global
		global $loopDirFlag;
	}
}

function write_scss() {


}

function scanLoop( $nDirectory, $inner, $file, $time) {
	if( $file !== "." && $file !== ".." && $file !== "stat.txt") {

		// Get the timestamp for the current file
		$temp = filemtime( __dir__ . $inner . $nDirectory["keyword"] . "/" . $file);
			
		// Compare the two timestamps
		if( $temp !== $time && ( strpos( $file, ".scss") || strpos( $file, '.js') || strpos( $file, '.txt') || strpos( $file, '.php'))) {

			// If this is not a directory, call the recursive function to scan directory
			$flag = true;
		
		}
		// If timestamps match, check to see if this is directory
		else {
			if( is_dir( __dir__ . $inner . $nDirectory["keyword"] . "/" . $file) && !$flag) {

				// Check to see if a new taxonomy needs to be created
				createTax( array( 'keyword' => $file, 'taxonomy_name' => $nDirectory["taxonomy_name"]), $inner . $nDirectory["keyword"]);

				// Recursively call the function
				$flag = scanDirectoriesForNewFiles( array( 'keyword' => $file, "taxonomy_name" => $nDirectory["taxonomy_name"]), $inner . $nDirectory["keyword"] . "/", $time);
					// var_dump();	
					

			} 
		}	

		return $flag;
	}
}

?>