<?php 
function module_type_taxonomy() {
    $labels = array(
        'name'                       => _x('Module Types', 'Taxonomy General Name', 'Baker Design Theme 1.0'),
        'singular_name'              => _x('Module Type', 'Taxonomy Singular Name', 'Baker Design Theme 1.0'),
        'menu_name'                  => __('Module Types', 'Baker Design Theme 1.0'),
        'all_items'                  => __('All Module Types', 'Baker Design Theme 1.0'),
        'parent_item'                => __('Parent Module Type', 'Baker Design Theme 1.0'),
        'parent_item_colon'          => __('Parent Module Type:', 'Baker Design Theme 1.0'),
        'new_item_name'              => __('New Module Type Name', 'Baker Design Theme 1.0'),
        'add_new_item'               => __('Add New Module Type', 'Baker Design Theme 1.0'),
        'edit_item'                  => __('Edit Module Type', 'Baker Design Theme 1.0'),
        'update_item'                => __('Update Module Type', 'Baker Design Theme 1.0'),
        'view_item'                  => __('View Module Type', 'Baker Design Theme 1.0'),
        'separate_items_with_commas' => __('Separate Module Types with commas', 'Baker Design Theme 1.0'),
        'add_or_remove_items'        => __('Add or remove Module Types', 'Baker Design Theme 1.0'),
        'choose_from_most_used'      => __('Choose from the most used', 'Baker Design Theme 1.0'),
        'popular_items'              => __('Popular Module Types', 'Baker Design Theme 1.0'),
        'search_items'               => __('Search Module Types', 'Baker Design Theme 1.0'),
        'not_found'                  => __('Not Found', 'Baker Design Theme 1.0'),
    );
    $args = array(
        'labels'            => $labels,
        'hierarchical'      => true,
        'public'            => true,
        'show_ui'           => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => true,
        'show_tagcloud'     => true,
    );
    register_taxonomy('module_type_taxonomy', array('module'), $args);

}
add_action('init', 'module_type_taxonomy', 0);

