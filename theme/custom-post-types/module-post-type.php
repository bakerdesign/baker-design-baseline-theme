<?php 
// Register Module Post Type
function module_post_type() {
    $labels = array(
        'name'               => _x('Modules', 'Post Type General Name', 'Baker Design Theme 1.0'),
        'singular_name'      => _x('Module', 'Post Type Singular Name', 'Baker Design Theme 1.0'),
        'menu_name'          => __('Modules', 'Baker Design Theme 1.0'),
        'name_admin_bar'     => __('Modules', 'Baker Design Theme 1.0'),
        'parent_item_colon'  => __('Parent Module:', 'Baker Design Theme 1.0'),
        'all_items'          => __('All Modules', 'Baker Design Theme 1.0'),
        'add_new_item'       => __('Add New Module', 'Baker Design Theme 1.0'),
        'add_new'            => __('Add Module', 'Baker Design Theme 1.0'),
        'new_item'           => __('New Module', 'Baker Design Theme 1.0'),
        'edit_item'          => __('Edit Module', 'Baker Design Theme 1.0'),
        'update_item'        => __('Update Module', 'Baker Design Theme 1.0'),
        'view_item'          => __('View Module', 'Baker Design Theme 1.0'),
        'search_items'       => __('Search Module', 'Baker Design Theme 1.0'),
        'not_found'          => __('Not found', 'Baker Design Theme 1.0'),
        'not_found_in_trash' => __('Not found in Trash', 'Baker Design Theme 1.0'),
    );
    $args = array(
        'label'               => __('Module', 'Baker Design Theme 1.0'),
        'description'         => __('Module Post Type', 'Baker Design Theme 1.0'),
        'labels'              => $labels,
        'supports'            => array('title', 'revisions'),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => true,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );
    register_post_type('module', $args);

}
add_action('init', 'module_post_type', 0);

my_require('module-type-taxonomy.php');