<?php
function custom_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Custom Taxonomies', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Custom Taxonomy', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Custom Taxonomies', 'text_domain' ),
		'all_items'                  => __( 'All Custom Taxonomies', 'text_domain' ),
		'parent_item'                => __( 'Parent Custom Taxonomy', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Custom Taxonomy:', 'text_domain' ),
		'new_item_name'              => __( 'New Custom Taxonomy', 'text_domain' ),
		'add_new_item'               => __( 'Add New Custom Taxonomy', 'text_domain' ),
		'edit_item'                  => __( 'Edit Custom Taxonomy', 'text_domain' ),
		'update_item'                => __( 'Update Custom Taxonomy', 'text_domain' ),
		'view_item'                  => __( 'View Custom Taxonomy', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Items', 'text_domain' ),
		'search_items'               => __( 'Search Items', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
	);
	$rewrite = array(
		'slug' => 'taxonomy',
		'hierarchical' => true,
		'with_front' => true,
		'query_var' => true
	);

	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'rewrite'            		 => $rewrite,
	);
	register_taxonomy( 'custom_taxonomy', array( 'custom_post_type' ), $args );

}
add_action( 'init', 'custom_taxonomy', 0 );