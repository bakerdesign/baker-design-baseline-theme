<?php
// Register Custom Post Type
function custom_post_type() {
	$labels = array(
		'name'                => _x( 'Custom Post Types', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Job Opening', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Custom Post Types', 'text_domain' ),
		'name_admin_bar'      => __( 'Job Opening', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
		'all_items'           => __( 'All Custom Post Types', 'text_domain' ),
		'add_new_item'        => __( 'Add New Custom Post Type', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'new_item'            => __( 'New Custom Post Type', 'text_domain' ),
		'edit_item'           => __( 'Edit Custom Post Type', 'text_domain' ),
		'update_item'         => __( 'Update Custom Post Type', 'text_domain' ),
		'view_item'           => __( 'View Custom Post Type', 'text_domain' ),
		'search_items'        => __( 'Search Custom Post Type', 'text_domain' ),
		'not_found'           => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	);

$args = array(
		'label'               => __( 'Custom Post Types', 'text_domain' ),
		'description'         => __( 'Custom Post Types Custom Post Type', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', ),
		'taxonomies'          => array( 'departments', 'job_type', 'companies'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-groups',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'rewrite'             => array( 'slug' => 'cpt', 'with_front' => true ),
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'query_var' 		  => true
	);

	register_post_type( 'custom_post_type', $args );
}
add_action( 'init', 'custom_post_type', 0 );

//Include Taxonomies Related to this post type
require_once dirname(__FILE__) . '/custom-taxonomy.php';