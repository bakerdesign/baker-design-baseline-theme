/**
 * General JavaScripture
 * @package WordPress
 * @subpackage Baker Design Theme 1.0
 * @since 2.0
 **/

Project.generalJavaScripture = function() {
    var disabledOverlays = $('.disabled-overlay'),
        hostname = new RegExp(location.host);

    // Useful for iframes & interactive embedded content (like maps)
    disabledOverlays.on('click', function(event) {
        $(this).addClass('enabled');
    });

    // Add 'target="_blank"' attribute to all external links
    $('a[href]').each(function() {
        var url = $(this).attr('href');
        if (hostname.test(url)) {
            $(this).addClass('internal-link');
        } else if (url.slice(0, 1) == '/') {
            $(this).addClass('internal-link');
        } else if (url.slice(0, 1) == '#') {
            $(this).addClass('anchor-link');
        } else if (url.indexOf('javascript') !== 0 ){
            $(this).addClass('external-link').attr('target', '_blank');
        }
    });

    // Remove empty paragraphs
    // $('p').each(function() {
    //     if ($(this).html().replace(/\s|&nbsp;/g, '').length === 0) {
    //         $(this).remove();
    //     }
    // });
};