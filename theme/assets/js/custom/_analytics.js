/**
 * Analytics-Related Javascript Functions
 * @package WordPress
 * @subpackage Baker Design Theme 1.0
 * @since 2.0
 **/
/**
 * Manually set & push analytics tracking info to Google
 *
 * @param child element
 * @uses ga (Google Analytics), pageTitle()
 */
Project.analyticsTrack = function(path, title) {
    var docTitle = title || pageTitle(path);
    ga('set', {
        page: path,
        title: docTitle
    });
    ga('send', 'pageview');
    if (document.location.hash === "#test") {
        console.log('tracked: ' + path + ' ~ ' + docTitle);
    }
};