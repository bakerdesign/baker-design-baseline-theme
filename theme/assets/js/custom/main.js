/**
 * Init JavaScript - This script is executed last in the final compiled app.js
 *
 * @package WordPress
 * @subpackage Baker Design Theme 1.0
 * @since 2.0
 **/

// $ = jQuery;

// Variables
var initGlobals = new Project.initGlobals();
var initGeneralScripts = new Project.generalJavaScripture();