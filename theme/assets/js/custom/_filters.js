/**
 * Filter related JS functions and methods
 *
 * @package WordPress
 * @subpackage Baker Design Theme 1.0
 * @since 2.0
 **/

$ = jQuery;

Project.Filters = Project.Filters || {};

Project.Filters.initiateTextSearchFilter = function(inputSelector, searchItemsSelector) {
    $(inputSelector).on('keyup kepress', function(event) {
        var searchText = $(this).val().toLowerCase();
        if (searchText !== '') {
            $(searchItemsSelector).filter(":not(:Contains(" + searchText +
                "))").hide();
            $(searchItemsSelector).filter(":Contains(" + searchText + ")").show();
        } else {
            $(searchItemsSelector).show();
        }
      });
};