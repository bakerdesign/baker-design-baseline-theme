/**
 * Theme Common Javascript Functions
 * @package WordPress
 * @subpackage Baker Design Theme 1.0
 * @since 2.0
 **/

/**
 * Push history state to browser and update page title
 *
 * @param variable name as string
 * @return Bool
 * @uses Project.pageTitle()
 * @usage pushHistoryState('/new-url/');
 */
Project.pushHistoryState = function(permalink) {
    if (history.pushState) {
        window.history.pushState(null, permalink.replace('/', ''), permalink);
        $.ajax({
            url: permalink,
            complete: function(data) {
                Project.pageTitle(permalink);
            }
        });
    }
};

/**
 * Check if mobile
 *
 * @param variable name as string
 * @return Bool
 * @uses Project.pageTitle()
 * @usage pushHistoryState('/new-url/');
 */
Project.isMobile = function() {
    return ($(window).width() <= Project.globals.mediumBreak);
};

Project.getParameterByName = function(name, url) {
    if (typeof url === 'undefined') {
        url = window.location.href;
    }
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) {
        return null;
    }
    if (!results[2]) {
        return '';
    }
    return decodeURIComponent(results[2].replace(/\+/g, " "));
};

Project.scrollToElement = function(selector, time, overrideselector) {
    if (typeof selector === 'undefined') {
        selector = 'body';
    }
    if (typeof overrideselector === 'undefined') {
        overrideselector = 'html, body';
    }
    if (typeof time === 'undefined') {
        time = 1000;
    }
    if ($(selector).length > 0) {
        $(overrideselector).animate({
            scrollTop: $(selector).offset().top + -80
        }, time);
    }
};

Project.addIEVersionClasses = function() {
    var ua = window.navigator.userAgent,
        msie = ua.indexOf("MSIE ");
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        $('html').addClass('ie' + (parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)))));
    }
    return false;
};

Project.equalHeights = function() {
    var equalHeights = $('.equal-heights');
    windowWidth = $(window).width();
    if (windowWidth > Project.globals.mediumBreak) {
        if (equalHeights.length > 0) {
            equalHeights.each(function() {
                var sections = $(this).find('.equal'),
                    largest_height = 0;
                sections.height('auto');
                sections.each(function() {
                    var h = $(this).height();
                    if (largest_height < h) {
                        largest_height = h;
                    }
                });
                sections.css('height', largest_height + 'px');
            });
        }
    } else {
        equalHeights.find('.equal').css('height', 'auto');
    }
};


Project.equalWidths = function(useSmallest) {
    var compareSizes = function (a, b) {
        return (Project.isSet('useSmallest') ? useSmallest : false) ? (a > b) : (a < b);
    };
    var equalWidths = $('.equal-widths');
    equalWidths.removeClass('equalized');
    windowWidth = $(window).width();
    if (windowWidth > Project.globals.mediumBreak) {
        if (equalWidths.length > 0) {
            equalWidths.each(function() {
                var sections = $(this).find('.equal'),
                    useWidth = 0;

                sections.width('auto');

                if ( sections.length > 1 ) {
                    sections.each(function() {
                        var h = $(this).innerWidth();
                        if (compareSizes(useWidth, h)) {
                            useWidth = h;
                        }
                    });
                    sections.css('width', useWidth + 'px');
                }

                $(this).addClass('equalized');
            });
        }
    } else {
        equalWidths.find('.equal').css('width', 'auto');
    }
};

Project.equalHeightsMobile = function() {

    var equalHeights = $('.equal-heights-mobile');

    windowWidth = $(window).width();
    if (windowWidth <= Project.globals.mediumBreak) {
        if (equalHeights.length > 0) {
            equalHeights.each(function() {
                var sections = $(this).find('.equal'),
                    largest_height = 0;
                $(this).removeClass('equalized');
                sections.height('auto');
                sections.each(function() {
                    var h = $(this).innerHeight();
                    if (largest_height < h) {
                        largest_height = h;
                    }
                });
                sections.css('height', largest_height + 'px');
                $(this).addClass('equalized');
            });
        }
    } else {
        equalHeights.find('.equal').css('height', 'auto');
    }
};

/**
 * Return the first relative parent of an element
 *
 * @param child element
 * @return Object
 * @uses jQuery
 */
Project.closestRelativeParent = function(elem) {
    var rel = false,
        par = elem.parent();
    while (rel === false) {
        rel = (par.css('position') === 'relative' || par.is('body'));
        if (rel !== true) {
            par = par.parent();
        }
    }
    return par;
};

/**
 * Get page title of provided URL
 *
 * @param url
 * @return String
 */
Project.pageTitle = function(url, update) {
    var result = "";
    update = update || false;
    $.ajax({
        url: url,
        async: false,
        success: function(data) {
            result = data;
        }
    });
    if (update) {
        document.title = result.match(/<(title)>[\s\S]*?<\/\1>/gi)[0].replace('<title>', '').replace('</title>', '');
    }
    return result.match(/<(title)>[\s\S]*?<\/\1>/gi)[0].replace('<title>', '').replace('</title>', '');
};

/**
 * Check if variable is defined
 *
 * @param variable name as string
 * @return Bool
 * @usage isSet('varNameAsString')
 */
Project.isSet = function(varAsStr) {
    prevItem = window;
    varSplit = varAsStr.split('.');
    x = 0;
    if (typeof(window[varSplit[0]]) !== 'undefined') {
        obj = window[varSplit[0]];
    }
    $.each(varSplit, function(i, item) {
        prevItem = prevItem[item];
        if (i === (varSplit.length - 1)) {
            if (typeof(item) !== 'undefined') {
                retVal = (typeof(prevItem) !== 'undefined');
            } else {
                retVal = false;
            }
            return retVal;
        }
    });
};

/**
 * Appends an external JavaScript to the body of your HTML
 *
 * @param jsname - the URL of the external JavaScript file
 */
Project.addJavascript = function(jsname) {
    if (jsname.length > 0) {
        scriptElem = document.createElement('script');
        scriptElem.setAttribute('type', 'text/javascript');
        scriptElem.setAttribute('src', jsname);
        $('body').append(scriptElem);
    }
};

/**
 * Appends an external stylesheet to the body of your HTML
 *
 * @param cssname - the URL of the external CSS stylesheet
 */
Project.addCSS = function(cssname) {
    if (cssname.length > 0) {
        $('body').append('<link rel="stylesheet" href="' + cssname + '" />');
    }
};

/**
 * Returns the length of any type of object (or false for undefined types)
 *
 * @param  varObj - can be any type of object (JSON object, string, array, or whatever)
 * @return int
 */
Project.count = function(varObj) {
    var typeObj = (typeof varObj);
    if ( typeObj === 'undefined' ) {
        return false;
    } else if ( typeObj === 'object' ) {
        return Object.keys(varObj).length;
    } else {
        return varObj.length;
    }
};
