/**
 * Global JavaScript
 *
 * @package WordPress
 * @subpackage Baker Design Theme 1.0
 * @since 2.0
 **/

Project.globals = Project.globals || {};

Project.initGlobals = function() {
    Project.globals.themeDir = '/wp-content/themes/baker-design-baseline';
    Project.globals.currentPageHref = document.location.href;
    Project.globals.mediumBreak = 669;
    Project.globals.largeBreak = 1025;
};