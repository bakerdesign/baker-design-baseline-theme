<?php
/***********************************************************************

Baker Design Theme 1.0
Register Custom Post Types

/***********************************************************************/

/**
 * Loop through/register all custom-post types in specified directory
 *
 * @param [string] folder_name
 * @return void
 */
function register_custom_post_types($folder_name = 'custom-post-types') {
    $post_types_folder = get_stylesheet_directory() . '/' . $folder_name;
    if(file_exists($post_types_folder)) :
        if ($handle = opendir($post_types_folder)) :
            while (false !== ($file = readdir($handle))) :
                $custom_post_type = $post_types_folder . '/' . $file;
                if ( is_file($custom_post_type) && strpos($custom_post_type, '.php') !== false ):
                    require_once($custom_post_type);
                endif;
            endwhile;
            closedir($handle);
        endif;
    endif;
}

/** Register/Add all Filters/Actions */
register_custom_post_types();