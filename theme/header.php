<?php
/**
 * The Header for the Baker Design Baseline Theme
 *
 * Displays all of the header and navigation
 *
 * @package WordPress
 * @subpackage Baker Design Theme 1.0
 * @since 2.0
 */

my_require( dirname( __FILE__ ) . '/create-tax.php' );
$themeURL =  get_stylesheet_directory_uri();
global $module;
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<title><?php if( isset( $module['page']) && sizeof( $module["page"])) { echo $module["page"]->post_title;} else { $p = get_post(); echo $p->post_title;} ?> | <?php echo bloginfo( "name"); ?> | <?php echo bloginfo( "description"); ?></title>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<!-- force IE out of compatability mode for any version -->
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<!-- force mobile view 100% -->
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link href="<?php echo $themeURL; ?>/assets/img/favicon.png" rel="shortcut icon" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<header role="banner" class="wrap">
	    <?php site_logo(); ?>
	    <nav id="primary-nav" role="navigation">
	      <!-- <a href="#content" class="skip-link screen-reader-text"><?php _e( 'Skip to main content', '%Text_Domain%' ); ?></a> -->
	      <ul>
	      	<?php primary_navigation(); ?>
	    	</ul>
	    </nav><!-- #primary-nav -->
	</header>
	<main> <!-- Site container --> 