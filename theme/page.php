<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Baker Design Theme 1.0
 * @since 2.0
 **************************************************************** 
 ****************************************************************/

// Initiate the global variable
global $module;

// Get the page data
$module["page"] = get_post();
$module["page_fields"] = get_fields( $module["page"]->ID);
$module["module_fields"] = get_fields( 20);

get_header(); 
?>

<?php // Get the modules
if( is_array( $module["page_fields"]['modules'])) {
	
	// If there are modules, loop through them
	foreach( $module["page_fields"]['modules']  as $this_module) {
		
		$module["module"] = $this_module;
		// Get the module fields
		$module["module_fields"] = get_fields( $this_module->ID);

		// Get the type of module
		$term = wp_get_post_terms( $this_module->ID, 'module_type_taxonomy');

		// Create the HTML Wrapper, using the term slug as a class
		// Use the post name for an ID 
		if( $term) { 

			// Create the class and template strings
			$term_str = "";
			$term_class = "";
			$term_val = "";

			// Get the term slug, split into array
			$term_array = explode( "_", $term[0]->slug);
			
			// Loop through slug, creating strings from the parts
			for( $i=0; $i<sizeof( $term_array); $i++) {
				$term_str = $term_str . "/" . $term_val . $term_array[$i];
				$term_class = $term_class . " module-" . $term_val . $term_array[$i];
				$term_val = $term_array[$i] . "_";
			}

			// Add final part to template string
			$term_str = $term_str . "/template"; ?>

			<div id="<?php echo $this_module->post_name; ?>" class="<?php echo $term_class; ?> <?php if ($module["module_fields"]["background_color"]) {echo $module["module_fields"]["background_color"];} else { echo "white-background";} ?>" >
				
				<?php // Get the module template
				get_template_part( "templates/" . $this_module->post_type . $term_str); ?>

			</div> <!--Module Name:  #<?php echo $this_module->post_name; ?> -->
		<?php } 
	}
} ?>

<?php
get_footer();
