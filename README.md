# README #

## Get Started ##

In order to compile our SCSS and JavaScript files this theme uses Grunt Task Manager. You can read how to [install Grunt here](https://gruntjs.com/getting-started). You will
also need [Node and NPM installed](https://docs.npmjs.com/getting-started/installing-node) to run grunt. 

## Running Grunt ##

You will open your terminal and cd into the theme directory. 
```cd /wp-content/themes/baker-design-baseline```

Then while in that directory in the terminal run the following command to install the node packages required to run the grunt tasks. 
```npm install``` 

After you have installed all the Node Packages you can run the following commands to compile the SCSS and javascript: 

* This will run grunt one time and copmile everything once. 
``` grunt ```

* If you want to watch your changes and compress the css and javascript use this command
``` grunt watch ```

* While in development it's typically best to run this command so you don't minify all your code while editing. 
``` grunt dev ```

## CSS, SCSS and Javascript ##

###  SCSS ###
There is a global SCSS folder located at assets/scss and in this folder you will have the following folders:
* globals - This is for all basic styling of type, links, forms and non layout specific styles. 
* settings - This is where all the scss variables, functions and mixins live. 
* vendor - anything that came from somewehre else such as fontawesome. 
* admin - wp admin styles 

There is a module specific starter SCSS that is located in ```/templates/module/module-template/``` and that can be edited for all module specific styles. 

###  JavaScript ###
There is a global JavaScript folder located at assets/js and in this folder you will have the following folders:
* custom - this is all the javascript that you can write 
* lib - this is all the plugins like slick slider 

There is a module specific starter JS that is located in ```/templates/module/module-template/``` and that can be edited for all module specific JavaScript. 

### Compiled Files ###

All SCSS is compiled with Grunt into two CSS files and all javaScript will be compiled into two JavaScript files. For CSS the file will be located in /assets/css/app.css and assets/css/admin.css. The javscript is compiled in /assets/js/app.js and /assets/js/lib.js.

Those files should never be edited because they will be overwritten on compile and they are ignored from the repo. 

## Creating Modules ##

1. To create a new module open the ``` /templates/module ``` directory and duplicate the module-template folder and name it the desired name. The format for parent needs to be parent-name. The format for children needs to be parent-name_child-name. Children of a parent are found/created within the parent template folder. The parent name needs to match the parent name folder. 

2. Refresh/reload the website to run the script that adds the file name to module types and this wil also allow for your scss file to be added to the SCSS import. 

3. In the dashboard, go to Module Types (under Module) and rename the Name for new module type to look more professional for the client’s use. The format needs to be; #00 Basic (for the parent) or #00a Basic - Two Column (for a child). We capitalize the title and put a hyphen between the parent name and the child name.

4. Open the files for the new module template and change the name in the comment section to match the module template name.

5. Create Custom Fields for the module template.

    * Name the Field Group to match the module template. The format needs to be Modules - #00 Basic (1000) for parent and Modules - #00a Basic - Two Column (1001) for children.

    * The number in the name and the order number (field at the bottom of the screen) match. They are created with the format: “1”, parent module template number, child number. So a Basic would be “1”, “00”, “0” and the first child of a basic module would be “1”, “00”, “1”. 

    * Change the Post Taxonomy choice to the new module type. This will connect the custom fields to the module and module template. 

6. Create a Module.

    * The name of the module needs to be in the format: Homepage - Basic Intro. The first part is the name of the page the module will be displayed and the second part is a descriptive name for the module. 

    * Check the box on the right side to choose the module type. This will connect this module to the module template files and the custom fields. 

* Enter content in the fields, and click “Publish”/”Update”.

7. Go to Pages and select the page the module will be displayed on.  

    * Add the module to the page by finding it in the left side column and clicking it. 

    * Drag and drop the module name to its correct order on the page. 

 8. Edit/write the code in the module template files. 
